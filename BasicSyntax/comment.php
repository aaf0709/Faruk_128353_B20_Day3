<?php

echo "This comment is for single line."; // single line comment

echo "<hr>";
/*
 * This
 *
 * is
 *
 * for
 *
 * multiple
 *
 * line.
 *
 * */


echo "This comment is for multiple line.";

echo "<hr>";

echo "This comment for single line."; # Single line comment

echo "<hr>";

?>