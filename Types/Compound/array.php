<?php

$a = array(5, 6, 7, 8); //indexed array
$b = array("a=>1", "b" => 2); //assosiative array #here "a=>1" will show string

echo "<pre>";
var_dump($a);
echo "</pre>";

echo "<pre>";
print_r($a);
echo "</pre>";

echo "<pre>";
var_dump($b);
echo "</pre>";

echo '<br />';

echo $a[0]; //only 0 label array will print

echo "<hr>";

$age = array("Peter" => "35", "Ben" => "37", "Joe" => "43");
echo "Peter is " . $age['Peter'] . " years old.";

echo "<hr>";

$cars = array("Volvo", "BMW", "Toyota");
echo "I like " . $cars[0] . ", " . $cars[1] . " and " . $cars[2] . ".";


?>