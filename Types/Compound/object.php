<?php
class MyClass{

}

$object = new MyClass();
echo gettype($object);

echo "<hr>";

class foo
{
    function do_foo()
    {
        echo "Doing foo.";
    }
}

$bar = new foo;
$bar->do_foo();

echo "<hr>";

$literalObjectDeclared = (object) array(
    'foo' => (object) array(
        'bar' => 'baz',
        'pax' => 'vax'
    ),
    'moo' => 'ui'
);
print $literalObjectDeclared->foo->bar; // outputs "baz"!

?>