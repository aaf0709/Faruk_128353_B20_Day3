<?php

$a = 1.23456789;
$b = 1.23456780;
$epsilon = 0.00001;


if (abs($a - $b) < $epsilon) {

    echo "true";
} else
    echo "false";

echo "<hr>";

$x = 10.365;
var_dump($x);

echo "<hr>";


$c = 1.23456789;
$d = 1.23456780;
$precision = 5;

if (bccomp($c, $d, $precision) === 0) {
    echo "true";
} // true


?>