<?php

var_dump((bool)"");        // bool(false)
echo "<hr>";
var_dump((bool)1);         // bool(true)
echo "<hr>";
var_dump((bool)-2);        // bool(true)
echo "<hr>";
var_dump((bool)"foo");     // bool(true)
echo "<hr>";
var_dump((bool)2.3e5);     // bool(true)
echo "<hr>";
var_dump((bool)array(12)); // bool(true)
echo "<hr>";
var_dump((bool)array());   // bool(false)
echo "<hr>";
var_dump((bool)"false");   // bool(true)


?>